import { createSelector } from "@ngrx/store";
import * as listReducer from "../passit-frontend/list/list.reducer";
import * as secretsReducer from "../passit-frontend/secrets/secrets.reducer";
import * as fromRoot from "../passit-frontend/app.reducers";

const getSecrets = createSelector(fromRoot.getSecretState, secretsReducer.getSecrets);

export const getSecretManaged = createSelector(
  listReducer.getListSecretManaged,
  getSecrets,
  (secretManaged, secrets) =>
    secrets.find((secret) => secret.id === secretManaged)
);