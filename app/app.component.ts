import { Component } from "@angular/core";

import { AppDataService } from "./passit-frontend/shared/app-data/app-data.service";

import { registerElement } from "nativescript-angular/element-registry";
registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);

@Component({
  selector: "ns-app",
  templateUrl: "app.component.html",
})
export class AppComponent {
  constructor(private appDataService: AppDataService) {
    this.appDataService.rehydrate();
  }
}
